﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_3_2
{
    public struct Param
    {
        public int x;
        public int y;
        public Param(int p1, int p2)
        {
            x = p1;
            y = p2;
        }
    }
    
    public class SetOfEquations
    {
        Param _p1;
        Param _p2;

        public SetOfEquations(string str1, string str2)
        {
            try
            {
                _p1 = SetOfEquations.parse(str1);
                _p2 = SetOfEquations.parse(str2);
            }
            catch (FormatException)
            {
                throw;
            }
            catch (ArgumentOutOfRangeException)
            {
                throw;
            }
        }

        static public Param parse(string st)
        {
            int a, b;
            string[] arrStr = st.Split(',', ' ');
            if (arrStr.Length != 2)
            {
                throw new ArgumentOutOfRangeException("The string must have 2 parameters");
            }
            if (!int.TryParse(arrStr[0], out a) || !int.TryParse(arrStr[1], out b))
            {
                throw new FormatException("The string must have only iteger type parameters");
            }
            else
            {
                return new Param(a, b);
            }
        }

       
        public void PrintResult(int x, int y)
        {
            Console.WriteLine("The set of equations:");
            Console.WriteLine("{0}x + {1}y = 0;", _p1.x, _p1.y);
            Console.WriteLine("{0}x + {1}y = 0;", _p2.x, _p2.y);
            Console.WriteLine("answers: x = {0}, y = {1} ", x, y);
            Console.WriteLine();
        }

        
        public void Calc(out int x, out int y)
        {
            if (_p1.x +_p1.y == 0 && _p2.x + _p2.y == 0)
            {
                throw new MyException("The set has a lot of solutions.  x = - y.");
            }
            else
            {
                x = 0;
                y = 0;
            }
        }
    }
}
