﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_3_2
{
    class Program
    {
        static public void Message(string str)
        {
            Console.WriteLine(str);
            Console.WriteLine("press any key for continue...");
            Console.ReadKey(); 
        }
        
        static void Main(string[] args)
        {
            SetOfEquations set; 
            bool isOk;
             do
             {
                Console.Clear();
                Console.WriteLine("Enter the first string with two integer parameters separated by \",\" or \" \"");
                string str1 = Console.ReadLine();
                Console.WriteLine("Enter the second string with two integer parameters separated by \",\" or \" \"");
                string str2 = Console.ReadLine();                 
                try
                {                   
                    set = new SetOfEquations(str1, str2);
                    isOk = true;
                    
                    try 
                    {
                        int x, y;
                        set.Calc(out x, out y);
                        set.PrintResult(x, y);
                    }
                    catch (MyException e)
                    {
                        Message(e.Message);
                        isOk = false;
                    }
                }
                catch (FormatException e)
                {
                    Message(e.Message);
                    isOk = false;
                }
                catch (ArgumentOutOfRangeException e)
                {
                    Message(e.Message);
                    isOk = false;                    
                }
             }
            while (!isOk);        
        }
    }
}
